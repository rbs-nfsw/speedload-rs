#[macro_use]
extern crate serde_derive;
extern crate reqwest;
extern crate serde_xml_rs as xml;
extern crate xz2;
extern crate md5;
extern crate base64;
extern crate rayon;
extern crate blake2;

use rayon::prelude::*;
use std::fs;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;
use std::time;
use std::ops::Deref;
use blake2::Digest;

#[derive(Default, Debug, Deserialize)]
#[serde(rename = "fileinfo")]
struct FileInfo {
    pub path: String,
    pub file: String,
    pub hash: String,
    pub revision: u64,
    pub section: u64,
    pub offset: u64,
    pub length: u64,
    #[serde(rename = "compressed")]
    pub compressed_length: Option<u64>,
}

#[derive(Default, Debug, Deserialize)]
struct Index {
    #[serde(rename = "fileinfo")]
    pub file_infos: Vec<FileInfo>,
}

fn main() {
    let client = reqwest::Client::new();
    println!("Loading Index");
    let start_time_t = time::Instant::now();
    let mut res = client
        .get("http://static.cdn.ea.com/blackbox/u/f/NFSWO/1614b/client/index.xml")
        .send()
        .unwrap();
    res.read_exact(&mut [0; 3]); // tbh it's really shitty way of stripping BOM
    let mut index_bytes = Vec::new();
    res.read_to_end(&mut index_bytes).unwrap();
    let hash = {
        let mut b2 = blake2::Blake2b::new();
        b2.input(&index_bytes);
        b2.result()
    };
    println!("Manifest hash: {:x}", hash);
    let index: Index = xml::deserialize(index_bytes.as_slice()).unwrap();
    println!("Index loaded in {}s", duration_secs(start_time_t.elapsed()));
    (&index.file_infos).into_iter().for_each(|fi| {
        let dir_path = Path::new("./game_files")
            .join(fi.path.trim_left_matches("CDShift").trim_left_matches("/"));
        if let Ok(meta) = fs::metadata(dir_path.join(&fi.file)) {
            if meta.len() == fi.length {
                //println!("Skipping file {}/{}", fi.path, fi.file);
                return;
            }
        }
        println!("Downloading file {}/{}", fi.path, fi.file);
        fs::create_dir_all(&dir_path).unwrap();
        let mut f = File::create(dir_path.join(&fi.file)).unwrap();

        let mut dec_clone = f.try_clone().unwrap();
        let mut decoder = xz2::write::XzDecoder::new_stream(
            &mut dec_clone,
            xz2::stream::Stream::new_lzma_decoder(u64::max_value()).unwrap(),
        );

        let length = fi.compressed_length.unwrap_or(fi.length);
        let mut to_read = length;
        let mut offset = fi.offset;
        let mut section = fi.section;
        let start_time = time::Instant::now();
        while to_read > 0 {
            let mut data = fetch_section(&client, section, offset, offset + to_read - 1);
            if fi.compressed_length.is_some() {
                to_read -= io::copy(&mut data, &mut decoder).unwrap();
            } else {
                to_read -= io::copy(&mut data, &mut f).unwrap();
            }
            section += 1;
            offset = 0;
        }
        println!("{} MB/s", length as f64 / 1024.0 / 1024.0 / duration_secs(start_time.elapsed()));
    });
    let start_time = time::Instant::now();
    (&index.file_infos).par_iter().for_each(|fi| {
        let dir_path = Path::new("./game_files")
            .join(fi.path.trim_left_matches("CDShift").trim_left_matches("/"));
        let mut f = File::open(dir_path.join(&fi.file)).unwrap();
        let mut bytes = Vec::new();
        f.read_to_end(&mut bytes).unwrap();
        let hash = md5::compute(bytes);
        let hashS = base64::encode(&*hash);
        if hashS != fi.hash {
            println!("Failed to verify {}/{}", fi.path, fi.file);
        }
    });
    println!("Verify done in {}s", duration_secs(start_time.elapsed()));
}

fn duration_secs(d: time::Duration) -> f64 {
    d.as_secs() as f64 + d.subsec_nanos() as f64 * 1e-9
}

fn fetch_section(client: &reqwest::Client, section: u64, from: u64, to: u64) -> Result<impl std::io::Read, ()> {
    client
        .get(&format!(
            "http://static.cdn.ea.com/blackbox/u/f/NFSWO/1614b/client/section{}.dat",
            section
        ))
        .header(reqwest::header::Range::Bytes(vec![
            reqwest::header::ByteRangeSpec::FromTo(from, to),
        ]))
        .send()?
}
